<?php

return [
    'language:download' => [
        'vendor' => 'clilanghelper',
        'class' => \Jpmschuler\CliLangHelper\Command\LanguagePackDownloader::class,
        'schedulable' => true,
        'runLevel' => \Helhum\Typo3Console\Core\Booting\RunLevel::LEVEL_MINIMAL,
    ],
];
