<?php

declare(strict_types=1);

namespace Jpmschuler\CliLangHelper\Service;

use Symfony\Component\Finder\Finder;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\EventDispatcher\EventDispatcher;
use TYPO3\CMS\Core\Http\RequestFactory;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Package\PackageManager;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;

/**
 * Service class handling language pack details
 * Used by 'manage language packs' module and 'language packs command'
 *
 * @internal This class is only meant to be used within EXT:install and is not part of the TYPO3 Core API.
 */
class LanguagePackService extends \TYPO3\CMS\Install\Service\LanguagePackService
{
    private const OLD_LANGUAGE_PACK_URLS = [
        'https://typo3.org/fileadmin/ter/',
        'https://beta-translation.typo3.org/fileadmin/ter/',
        'https://localize.typo3.org/fileadmin/ter/'
    ];
    private const LANGUAGE_PACK_URL = 'https://localize.typo3.org/xliff/';
    protected $languagePacksBaseUrl = '';

    public function __construct()
    {
        parent::__construct(
            GeneralUtility::makeInstance(EventDispatcher::class),
            GeneralUtility::makeInstance(RequestFactory::class),
            GeneralUtility::makeInstance(LogManager::class)->getLogger(self::class)
        );
    }

    /**
     * Create a list of loaded extensions and their language packs details
     *
     * @return array
     */
    public function getExtensionLanguagePackDetails(): array
    {
        $activeLanguages = $this->getActiveLanguages();
        $packageManager = GeneralUtility::makeInstance(PackageManager::class);
        $activePackages = $packageManager->getActivePackages();
        $extensions = [];
        $activeExtensions = [];
        foreach ($activePackages as $package) {
            $path = $package->getPackagePath();
            $finder = new Finder();
            try {
                $files = $finder->files()->in($path . 'Resources/Private/Language/')->name('*.xlf');
                if ($files->count() === 0) {
                    // This extension has no .xlf files
                    continue;
                }
            } catch (\InvalidArgumentException $e) {
                // Dir does not exist
                continue;
            }
            $key = $package->getPackageKey();
            $activeExtensions[] = $key;
            $title = $package->getValueFromComposerManifest('description') ?? '';
            if (is_file($path . 'ext_emconf.php')) {
                $_EXTKEY = $key;
                $EM_CONF = [];
                include $path . 'ext_emconf.php';
                $title = $EM_CONF[$key]['title'] ?? $title;

                $state = $EM_CONF[$key]['state'] ?? '';
                if ($state === 'excludeFromUpdates') {
                    continue;
                }
            }
            $extension = [
                'key' => $key,
                'title' => $title,
            ];
            if (!empty(ExtensionManagementUtility::getExtensionIcon($path, false))) {
                $extension['icon'] = PathUtility::stripPathSitePrefix(
                    ExtensionManagementUtility::getExtensionIcon($path, true)
                );
            }
            $extension['packs'] = [];
            foreach ($activeLanguages as $iso) {
                $isLanguagePackDownloaded = is_dir(Environment::getLabelsPath() . '/' . $iso . '/' . $key . '/');
                $lastUpdate = 0;
                $extension['packs'][] = [
                    'iso' => $iso,
                    'exists' => $isLanguagePackDownloaded,
                    'lastUpdate' => $this->getFormattedDate($lastUpdate),
                ];
            }
            $extensions[] = $extension;
        }
        usort($extensions, function ($a, $b) {
            // Sort extensions by key
            if ($a['key'] === $b['key']) {
                return 0;
            }
            return $a['key'] < $b['key'] ? -1 : 1;
        });
        return $extensions;
    }

    /**
     * Download and unpack a single language pack of one extension.
     *
     * @param string $key Extension key
     * @param string $iso Language iso code
     *
     * @return string One of 'update', 'new' or 'failed'
     * @throws \RuntimeException
     */
    public function languagePackDownload(string $key, string $iso): string
    {
        // Sanitize extension and iso code
        $availableLanguages = $this->getAvailableLanguages();
        $activeLanguages = $this->getActiveLanguages();
        if (!array_key_exists($iso, $availableLanguages) || !in_array($iso, $activeLanguages, true)) {
            throw new \RuntimeException('Language iso code ' . (string)$iso . ' not available or active', 1520117054);
        }
        $packageManager = GeneralUtility::makeInstance(PackageManager::class);
        $activePackages = $packageManager->getActivePackages();
        $packageActive = false;
        foreach ($activePackages as $package) {
            if ($package->getPackageKey() === $key) {
                $packageActive = true;
                break;
            }
        }
        if (!$packageActive) {
            throw new \RuntimeException('Extension ' . (string)$key . ' not loaded', 1520117245);
        }

        $languagePackBaseUrl = $this->languagePacksBaseUrl;
        if (empty($languagePackBaseUrl)) {
            throw new \RuntimeException('Language pack baseUrl not found', 1520169691);
        }

        if (in_array($languagePackBaseUrl, self::OLD_LANGUAGE_PACK_URLS, true)) {
            $languagePackBaseUrl = self::LANGUAGE_PACK_URL;
        }

        $path = ExtensionManagementUtility::extPath($key);
        $majorVersion = explode('.', TYPO3_branch)[0];
        if (strpos($path, '/sysext/') !== false) {
            // This is a system extension and the package URL should be adapted to have different
            // packs per core major version
            // https://localize.typo3.org/xliff/b/a/backend-l10n/backend-l10n-fr.v9.zip
            $packageUrl = $key[0] . '/' . $key[1] . '/' . $key . '-l10n/' .
                $key . '-l10n-' . $iso . '.v' . $majorVersion . '.zip';
        } else {
            // Typical non sysext path, Hungarian:
            // https://localize.typo3.org/xliff/a/n/anextension-l10n/anextension-l10n-hu.zip
            $packageUrl = $key[0] . '/' . $key[1] . '/' . $key . '-l10n/' .
                $key . '-l10n-' . $iso . '.zip';
        }

        $absoluteLanguagePath = Environment::getLabelsPath() . '/' . $iso . '/';
        $absoluteExtractionPath = $absoluteLanguagePath . $key . '/';
        $absolutePathToZipFile = Environment::getVarPath() . '/transient/' . $key . '-l10n-' . $iso . '.zip';

        $packExists = is_dir($absoluteExtractionPath);

        $packResult = $packExists ? 'update' : 'new';

        $operationResult = false;
        try {
            $response = $this->requestFactory->request($languagePackBaseUrl . $packageUrl);
            if ($response->getStatusCode() === 200) {
                $languagePackContent = $response->getBody()->getContents();
                if (!empty($languagePackContent)) {
                    $operationResult = true;
                    if ($packExists) {
                        $operationResult = GeneralUtility::rmdir($absoluteExtractionPath, true);
                    }
                    if ($operationResult) {
                        GeneralUtility::mkdir_deep(Environment::getVarPath() . '/transient/');
                        $operationResult = GeneralUtility::writeFileToTypo3tempDir(
                            $absolutePathToZipFile,
                            $languagePackContent
                        ) === null;
                    }
                    $this->unzipTranslationFile($absolutePathToZipFile, $absoluteLanguagePath);
                    if ($operationResult) {
                        $operationResult = unlink($absolutePathToZipFile);
                    }
                }
            } else {
                $this->logger->warning(
                    sprintf(
                        'Requesting %s was not successful, got status code %d (%s)',
                        $languagePackBaseUrl . $packageUrl,
                        $response->getStatusCode(),
                        $response->getReasonPhrase()
                    )
                );
            }
        } catch (\Exception $e) {
            $operationResult = false;
        }
        if (!$operationResult) {
            $packResult = 'failed';
        }
        return $packResult;
    }

    /**
     * Update main language pack download location if possible.
     * Store to registry to be used during language pack update
     *
     * @return string
     */
    public function updateMirrorBaseUrl(): string
    {
        $repositoryUrl = 'https://repositories.typo3.org/mirrors.xml.gz';
        $downloadBaseUrl = false;
        try {
            $response = $this->requestFactory->request($repositoryUrl);
            if ($response->getStatusCode() === 200) {
                $xmlContent = @gzdecode($response->getBody()->getContents());
                if (!empty($xmlContent['mirror']['host']) && !empty($xmlContent['mirror']['path'])) {
                    $downloadBaseUrl = 'https://' . $xmlContent['mirror']['host'] . $xmlContent['mirror']['path'];
                }
            } else {
                $this->logger->warning(
                    sprintf(
                        'Requesting %s was not successful, got status code %d (%s)',
                        $repositoryUrl,
                        $response->getStatusCode(),
                        $response->getReasonPhrase()
                    )
                );
            }
        } catch (\Exception $e) {
            // Catch generic exception, fallback handled below
            $this->logger->error('Failed to download list of mirrors', ['exception' => $e]);
        }
        if (empty($downloadBaseUrl)) {
            // Hard coded fallback if something went wrong fetching & parsing mirror list
            $downloadBaseUrl = self::LANGUAGE_PACK_URL;
        }
        $this->languagePacksBaseUrl = $downloadBaseUrl;
        return $downloadBaseUrl;
    }
}
