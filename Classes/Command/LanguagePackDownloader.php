<?php

namespace Jpmschuler\CliLangHelper\Command;

use Jpmschuler\CliLangHelper\Service\LanguagePackService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class LanguagePackDownloader extends Command
{
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $result = 'Hello World';
        $locales = $input->getArgument('locales');
        foreach ($locales as $locale) {
            $result .= "\nlang requested: " . $locale;
        }
        $output->writeln($result);
        $languagePackService = GeneralUtility::makeInstance(LanguagePackService::class);
        $noProgress = $input->getOption('no-progress') || $output->isVerbose();
        $isos = (array)$input->getArgument('locales');
        $skipExtensions = (array)$input->getOption('skip-extension');

        // Condition for the scheduler command, e.g. "de fr pt"
        if (count($isos) === 1 && strpos($isos[0], ' ') !== false) {
            $isos = GeneralUtility::trimExplode(' ', $isos[0], true);
        }
        if (empty($isos)) {
            $isos = $locales;
        }

        $output->writeln(
            sprintf(
                '<info>Updating language packs of all activated extensions for locale(s) "%s"</info>',
                implode('", "', $isos)
            )
        );

        $extensions = $languagePackService->getExtensionLanguagePackDetails();

        if ($noProgress) {
            $progressBarOutput = new NullOutput();
        } else {
            $progressBarOutput = $output;
        }
        $progressBar = new ProgressBar($progressBarOutput, count($isos) * count($extensions));
        $languagePackService->updateMirrorBaseUrl();
        $hasErrors = false;
        foreach ($isos as $iso) {
            foreach ($extensions as $extension) {
                if (in_array($extension['key'], $skipExtensions, true)) {
                    continue;
                }
                if ($noProgress) {
                    $output->writeln(
                        sprintf(
                            '<info>Fetching pack for language "%s" for extension "%s"</info>',
                            $iso,
                            $extension['key']
                        ),
                        $output::VERBOSITY_VERY_VERBOSE
                    );
                }
                $result = $languagePackService->languagePackDownload($extension['key'], $iso);
                if ($noProgress) {
                    switch ($result) {
                        case 'failed':
                            $output->writeln(
                                sprintf(
                                    '<error>Fetching pack for language "%s" for extension "%s" failed</error>',
                                    $iso,
                                    $extension['key']
                                )
                            );
                            $hasErrors = true;
                            break;
                        case 'update':
                            $output->writeln(
                                sprintf(
                                    '<info>Updated pack for language "%s" for extension "%s"</info>',
                                    $iso,
                                    $extension['key']
                                )
                            );
                            break;
                        case 'new':
                            $output->writeln(
                                sprintf(
                                    '<info>Fetching new pack for language "%s" for extension "%s"</info>',
                                    $iso,
                                    $extension['key']
                                )
                            );
                            break;
                    }
                }
                $progressBar->advance();
            }
        }
        $progressBar->finish();

        // Flush language cache
        GeneralUtility::makeInstance(CacheManager::class)->getCache('l10n')->flush();

        return $hasErrors ? 1 : 0;
    }

    protected function configure()
    {
        $this
            ->setDescription('Download language packs.')
            ->setHelp('This command download language packs without the need for a database...')
            ->addArgument(
                'locales',
                InputArgument::IS_ARRAY | InputArgument::OPTIONAL,
                'Provide iso codes separated by space to update only selected language packs.' .
                ' Example `bin/typo3 language:download de ja`.',
                []
            )
            ->addOption(
                'no-progress',
                null,
                InputOption::VALUE_NONE,
                'Disable progress bar.'
            )
            ->addOption(
                'skip-extension',
                null,
                InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                'Skip extension. Useful for e.g. for not public extensions, which don\'t have language packs.',
                []
            );
    }

    /**
     * Initializes the command after the input has been bound and before the input
     * is validated.
     *
     * This is mainly useful when a lot of commands extends one main command
     * where some things need to be initialized based on the input arguments and options.
     *
     * @see InputInterface::bind()
     * @see InputInterface::validate()
     */
    protected function initialize(
        InputInterface $input,
        OutputInterface $output
    ) {
    }

    protected function interact(
        InputInterface $input,
        OutputInterface $output
    ) {
        if (!$input->getArgument('locales')) {
            $output->writeln('Please provide one or more (csv) language pack labels');
            $helper = $this->getHelper('question');
            $question = new Question('LOCALES: ');
            $langlist = $helper->ask($input, $output, $question);
            $input->setArgument('language', $langlist);
        }
    }
}
