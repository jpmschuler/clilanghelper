<?php
$EM_CONF['clilanghelper'] = [
    'title' => 'CLI Lang Helper',
    'description' => 'Extension for downloading language packs without db (e.g. pre-deployment)',
    'category' => 'plugin',
    'author' => 'Schuler, J. Peter M.',
    'author_email' => 'jpmschuler@googlemail.com',
    'constraints' => [
        'depends' => [
            'typo3' => '*',
        ],
    ]
];
